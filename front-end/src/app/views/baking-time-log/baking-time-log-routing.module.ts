import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BakingTimeLogComponent } from './baking-time-log.component';
import { AuthGurad } from '../../Services/auth-gurad.service';

const routes: Routes = [
  {
    path: '',
    component: BakingTimeLogComponent,
    canActivate: [AuthGurad],
    data: {
      title: 'BakingTimeLog'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BakingTimeLogRoutingModule {}
