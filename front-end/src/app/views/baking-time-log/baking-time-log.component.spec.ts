import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BakingTimeLogComponent } from './baking-time-log.component';

describe('BakingTimeLogComponent', () => {
  let component: BakingTimeLogComponent;
  let fixture: ComponentFixture<BakingTimeLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BakingTimeLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BakingTimeLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
