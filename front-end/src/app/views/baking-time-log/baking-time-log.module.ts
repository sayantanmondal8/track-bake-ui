// import { BakeTimeChartsComponent } from './bake-time-charts/bake-time-charts.component';
import { NgModule } from '@angular/core';

 import { BakingTimeLogComponent } from './baking-time-log.component';
 import { BakingTimeLogRoutingModule } from './baking-time-log-routing.module';
import { CommonModule } from '@angular/common';
// import { BakeChartsComponent } from './bake-charts/bake-charts.component';
// import { DashboardWidgetsComponent } from './dashboard-widgets/dashboard-widgets.component';
// import { GaugeChartComponent } from './gauge-chart/gauge-chart.component';
// import { GaugeChartModule } from 'angular-gauge-chart';
import {
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatCardModule,
  MatProgressSpinnerModule,
  MatMenuModule,
  MatIconModule,
  MatToolbarModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule
} from '@angular/material';

@NgModule({
  imports: [
    BakingTimeLogRoutingModule,
    CommonModule,
    MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatCardModule,
  MatProgressSpinnerModule,
  MatMenuModule,
  MatIconModule,
  MatToolbarModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule
  ],
  declarations: [ 
    BakingTimeLogComponent

    ]
})
export class BakingTimeLogModule { }
