import { Component, OnInit,ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import { DashboardService } from '../../Services/dashboard.service';
//import { DatePipe } from '@angular/common';
import { ChartModel } from '../../models/chart-model';

@Component({
  selector: 'app-baking-time-log',
  templateUrl: './baking-time-log.component.html',
  styleUrls: ['./baking-time-log.component.scss']
})
export class BakingTimeLogComponent implements OnInit {
  displayedColumns: string[] = ['logTime', 'bakingTime', 'productionLine', 'productName'];
  dataSource = new MatTableDataSource<bakeLogModel>(ELEMENT_DATA);
  public fromDateTime: Date = new Date();
  public toDateTime: Date  = new Date();


  constructor(private dSvc : DashboardService) { }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ngOnInit() {
    this.dataSource.paginator = this.paginator;

    const date = new Date();
    this.fromDateTime.setDate(date.getDate() - 180);
    this.toDateTime = new Date();

     this.dSvc.getBKData().subscribe((data:ChartModel[]) => {
      //console.log(data);
      for(let i =0; i<data.length; i++){
        console.log(data[i] +"here");
        let bkmodel : bakeLogModel = new bakeLogModel() ;
        bkmodel.bakingTime = data[i].bk_time;
        bkmodel.logTime = data[i].createAt;
        bkmodel.productionLine = data[i].lineid;
        bkmodel.productName = data[i].prodid;
        ELEMENT_DATA.push(bkmodel)
      }
     });

  }

}
export class bakeLogModel {
  logTime: string;
  bakingTime: number;
  productionLine: string;
  productName: string;
}

const ELEMENT_DATA: bakeLogModel[] = [

];